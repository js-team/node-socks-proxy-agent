# Installation
> `npm install --save @types/dns2`

# Summary
This package contains type definitions for dns2 (https://github.com/song940/node-dns#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/dns2.

### Additional Details
 * Last updated: Wed, 01 Mar 2023 16:32:33 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Tim Perry](https://github.com/pimterry).
